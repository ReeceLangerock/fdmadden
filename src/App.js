import React, { Component } from "react";
import "./App.css";
import styled from "styled-components";

import PlayerList from "./components/PlayerList";
import SelectedPlayers from "./components/SelectedPlayers";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remainingSalary: 56000,
      roster: {
        QB: undefined,
        HB: undefined,
        HB2: undefined,
        WR: undefined,
        WR2: undefined,
        WR3: undefined,
        TE: undefined,
        FLEX: undefined
      },
      count: 8,
      searchString: ''
    };
    this.handleSearch = this.handleSearch.bind(this)
    this.addPlayer = this.addPlayer.bind(this);
    this.clearPlayer = this.clearPlayer.bind(this);
    this.clearAll = this.clearAll.bind(this);
  }

  handleSearch(event){
    this.setState({ searchString: event.target.value });

  }

  clearAll(){
    this.setState({
      remainingSalary: 56000,
      roster: {
        QB: undefined,
        HB: undefined,
        HB2: undefined,
        WR: undefined,
        WR2: undefined,
        WR3: undefined,
        TE: undefined,
        FLEX: undefined
      },
      count: 8,
    })
  }

  clearPlayer(position) {
    const positionSalary = this.state.roster[position].price
    this.setState(prevState => ({
      roster: {
        ...prevState.roster,
        [position]: undefined
      },
      remainingSalary: prevState.remainingSalary + positionSalary,
      count: prevState.count +1
    }));
  }
  addPlayer(player) {
    const { remainingSalary, roster, count } = this.state;
    //first check price
    if (player.price > remainingSalary) {
      return;
    }
    // then check for open spot
    if (player.position === "QB" && roster.QB === undefined) {
      this.setState(prevState => ({
        roster: {
          ...prevState.roster,
          QB: player
        }
      }));
    } else if (player.position === "HB") {
      if (roster.HB === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            HB: player
          }
        }));
      } else if (roster.HB2 === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            HB2: player
          }
        }));
      } else if (roster.FLEX === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            FLEX: player
          }
        }));
      } else {
        return;
      }
    } else if (player.position === "WR") {
      if (roster.WR === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            WR: player
          }
        }));
      } else if (roster.WR2 === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            WR2: player
          }
        }));
      } else if (roster.WR3 === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            WR3: player
          }
        }));
      } else if (roster.FLEX === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            FLEX: player
          }
        }));
      } else {
        return;
      }
    } else if (player.position === "TE") {
      if (roster.TE === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            TE: player
          }
        }));
      } else if (roster.FLEX === undefined) {
        this.setState(prevState => ({
          roster: {
            ...prevState.roster,
            FLEX: player
          }
        }));
      } else {
        return;
      }
    } else {
      return;
    }

    this.setState({
      remainingSalary: remainingSalary - player.price,
      count: count - 1
    });
  }
  render() {
    const { remainingSalary, count, roster, searchString } = this.state;
    return (
      <GridContainer>
        <header >Revo DraftKings</header>
        <PlayerList onClick={this.addPlayer} searchString = {searchString} roster = {roster} handleSearch = {this.handleSearch}/>
        <SelectedPlayers clearAll = {this.clearAll} onClick={this.clearPlayer} remainingSalary={remainingSalary} count={count} roster={roster} />
      </GridContainer>
    );
  }
}

const GridContainer = styled.div`
  background-color: #282c34;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 25% 25% 5% 35%;
  grid-template-rows: auto;
  grid-template-areas:
    "header header header header"
    "main main . sidebar"
    "footer footer footer footer";
  flex-direction: column;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;

  @media (max-width: 768px) {
    grid-template-areas:
    "header"
    "main"
    "sidebar"
    "footer";
  flex-direction: column;
  grid-template-columns: 98%;
   
  }
`;

export default App;
