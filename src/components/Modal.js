import React, { Component } from "react";
import styled from "styled-components";

class Modal extends Component {
  render() {
    const { visible, modalType, onClick } = this.props;
    return (
      <ModalContainer modalType={modalType === "success"} visible={visible} onClick={() => onClick()}>
        {modalType === "form" && [
          <h1>You fucked something up</h1>,
          <p>Make sure your team name and submission code are entered correctly and that you have a full roster</p>,
          <p>Click inside this message to close, dumbass</p>
        ]}
        {modalType === "submission" && [
          <h1>There was an error submitting your entry</h1>,
          <p>Try again in a minute, if it doesn't work again I guess send me a pm. </p>
        ]}
        {modalType === "success" && [
          <h1>Your submission was successful!</h1>,
          <p>You can go the the Revo Deepdive page to see it!</p>
        ]}
      </ModalContainer>
    );
  }
}

const ModalContainer = styled.div`
width: 50%;
position: absolute;
background: white;
border: 5px solid ${props => props.modalType ? 'green':'#7e0100'};
color: ${props => props.modalType ? 'green':'#7e0100'};
text-align: center;
display: ${props => (props.visible ? "block" : "none")};
font-size: 20px;
left: 0;
min-width: 250px;
transform: translate(50vw, 50vh) translate(-50%, -50%);
box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.16), 0 0 0 1px rgba(0, 0, 0, 0.08);

h1 {
    font-size: 30px;
}
`;

export default Modal;
