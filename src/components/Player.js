import React, { Component } from "react";
import styled from "styled-components";

class Player extends Component {
  render() {
    if (this.props.empty) {
      return (
        <PlayerContainer>
          <Field>{this.props.position} Add Player</Field>
        </PlayerContainer>
      );
    }
    if (this.props.dummy) {
      return (
        <DummyContainer>
          <DummyField flex={3}>Name</DummyField>
          <DummyField flex={2}>Team</DummyField>
          <DummyField>Played</DummyField>
          <DummyField>FPPG</DummyField>
          <DummyField>Salary</DummyField>
        </DummyContainer>
      );
    }
    const { data } = this.props;
    let GP = 0;
    if(data.fpArray){

      data.fpArray.forEach(num => {
        if (num && num !== 0 && typeof num !== "undefined") {
          GP += 1;
        }
      });
    }
    if(data.rookie){
      GP = 'R'
    }
    return (
      <PlayerContainer pointer="pointer" onClick={() => this.props.onClick(data)}>
        <Field flex={3}>
          <Pos>{data.position}</Pos>{data.firstName} {data.lastName}
        </Field>
        <Field flex={2}>{data.teamName}</Field>
        <Field>{GP}</Field>
        <Field>{(data.fantasyPoints / GP).toFixed(1)}</Field>
        <Field>{data.price}</Field>
      </PlayerContainer>
    );
  }
}

const DummyField = styled.div`
  font-weight: bold;
  font-size: 16px;
  flex: ${props => props.flex || 1};
  align-self: center;
`;

const DummyContainer = styled.div`
  border: 1px solid;
  background: rgb(245,245,245);
  padding: 3px 1px;
  color: black;
  display: flex;
  justify-content: space-between;
`;

const Pos = styled.span`
background: rgb(228,232,238);
padding: 2px;
font-size: 12px;
margin-right: 2px;
font-weight: bold;`

const PlayerContainer = styled.div`
  border: 1px solid;
  background: white;
  color: black;
  display: flex;
  justify-content: space-between;
  min-height: 40px;
  cursor: ${props => props.pointer || "default"};
`;

const Field = styled.div`
  font-size: 18px;
  align-self: center;

  flex: ${props => props.flex || 1};
`;

export default Player;
