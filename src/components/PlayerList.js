import React, { Component } from "react";
import styled from "styled-components";

import players from "./../result.json";
import Player from "./Player";
import PositionContainer from "./PositionContainer";

class PlayerList extends Component {
  constructor(props) {
    super(props);
    this.selectPositionGroup = this.selectPositionGroup.bind(this);
    this.state = {
      selectedPositionGroup: "ALL"
    };
  }

  renderPlayers() {
    const { roster, onClick, searchString } = this.props;
    const alreadySelectedPlayers = [];

    Object.keys(roster).forEach(player => {
      if (roster[player] && roster[player]._id !== undefined) {
        alreadySelectedPlayers.push(roster[player]._id);
      }
    });

    const sel = this.state.selectedPositionGroup;
    const regexp = new RegExp(searchString, 'i');
    return players.map(player => {
      if(regexp.test(player.lastName) === false && regexp.test(player.firstName) === false){
        return null
      }
      if (alreadySelectedPlayers.includes(player._id)) {
        return null
      }
      if (sel === "HB" && player.position !== "HB") {
        return null
      }

      if (sel === "QB" && player.position !== "QB") {
        return null
      }
      if (sel === "WR" && player.position !== "WR") {
        return null
      }
      if (sel === "TE" && player.position !== "TE") {
        return null
      }
      if (sel === "FLEX" && player.position === "QB") {
        return null
      }
      return <Player data={player} key={player._id} onClick={onClick} />;
    });
  }

  selectPositionGroup(group) {
    this.setState({
      selectedPositionGroup: group
    });
  }
  render() {
    return (
      <OuterContainer>
        <PositionContainer
          onClick={this.selectPositionGroup}
          selectedPositionGroup={this.state.selectedPositionGroup}
          handleSearch={this.props.handleSearch}
          searchString = {this.props.searchString}
          
        />
        <Player dummy />
        <Container>{this.renderPlayers()}</Container>
      </OuterContainer>
    );
  }
}

const OuterContainer = styled.div`
  border: 1px solid;
  grid-area: main;
  height: 90vh;
  overflow: hidden;
  @media (max-width: 768px) {
   height: 80vh;
   margin-bottom: 10px;
   
  }
`;
const Container = styled.div`
  overflow: auto;
  height: 100%;
`;

export default PlayerList;
