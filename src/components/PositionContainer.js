import React, { Component } from "react";
import styled from "styled-components";

class PositionContainer extends Component {
  render() {
    const { selectedPositionGroup, onClick, searchString, handleSearch } = this.props;
    return (
      <Container>
        <Position selected={selectedPositionGroup === "ALL"} onClick={() => onClick("ALL")}>
          All
        </Position>
        <Position selected={selectedPositionGroup === "QB"} onClick={() => onClick("QB")}>
          QB
        </Position>
        <Position selected={selectedPositionGroup === "WR"} onClick={() => onClick("WR")}>
          WR
        </Position>
        <Position selected={selectedPositionGroup === "HB"} onClick={() => onClick("HB")}>
          HB
        </Position>
        <Position selected={selectedPositionGroup === "TE"} onClick={() => onClick("TE")}>
          TE
        </Position>
        <Position selected={selectedPositionGroup === "FLEX"} onClick={() => onClick("FLEX")}>
          Flex
        </Position>

        <Search type ='text' placeholder = 'Find a player...' value = {searchString} onChange = {handleSearch}/>
     
      </Container>
    );
  }
}

const Container = styled.div`
  border: 1px solid;
  background: white;
  color: black;
  display: flex;
  justify-content: flex-start;
`;
const Position = styled.div`
  border: 1px solid;
  background: ${props => (props.selected ? "green" : "white")};
  color: black;
  padding: 0px 10px;
`;

const Search = styled.input`
margin-left: auto;
`

export default PositionContainer;
