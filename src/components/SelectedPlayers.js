import React, { Component } from "react";
import styled from "styled-components";

import Player from "./Player";
import Error from "./Modal";

class PlayerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamName: "",
      code: "",
      modalVisible: false,
      modalType: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleModal = this.handleModal.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { teamName, code } = this.state;
    const { roster, clearAll } = this.props;
    let validRoster = true;
    Object.keys(roster).forEach(position => {
      if (roster[position] === undefined) {
        validRoster = false;
      }
    });
    if (teamName.length < 4 || (code.length !== 6 && code !== "fun") || !validRoster) {
      console.log('fail')
      this.setState({
        modalVisible: true,
        modalType: "form"
      });
      return false;
    }

    const result = await fetch("http://www.deepdive.ml/api/dk", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        mode: "cors"
      },
      body: JSON.stringify({
        team: teamName,
        code: code,
        roster: roster
      })
    });
    var body = await result.json();
    if (body.success === "all good") {
      this.setState({
        modalVisible: true,
        modalType: "success"
      });
      clearAll();
    } else if (body.success === "bad code") {
      this.setState({
        modalVisible: true,
        modalType: "form"
      });
    } else {
      this.setState({
        modalVisible: true,
        modalType: "submission"
      });
    }
  }

  handleChange(event, field) {
    this.setState({ [field]: event.target.value });
  }
  handleModal() {
    this.setState({ modalVisible: false });
  }

  render() {
    const { remainingSalary, roster, count } = this.props;
    let avgSalaryRemaining = Math.round(remainingSalary / count / 100) * 100;
    if (count === 0) {
      avgSalaryRemaining = remainingSalary;
    }
    return (
      <Container>
        <Error onClick={this.handleModal} visible={this.state.modalVisible} modalType={this.state.modalType} />
        <SalaryContainer>
          Your Lineup
          <SubSal>
            <SalaryCol>
              <span>${remainingSalary.toLocaleString()}</span>
              <div>Salary Remaining</div>
              <span />
            </SalaryCol>
            <SalaryCol>
              <span>${avgSalaryRemaining.toLocaleString()}</span>
              <div>Avg/Player</div>
            </SalaryCol>
          </SubSal>
        </SalaryContainer>
        <Player
          empty={roster.QB === undefined}
          data={roster.QB}
          position="QB"
          onClick={() => this.props.onClick("QB")}
        />
        <Player
          empty={roster.HB === undefined}
          data={roster.HB}
          position="HB"
          onClick={() => this.props.onClick("HB")}
        />
        <Player
          empty={roster.HB2 === undefined}
          data={roster.HB2}
          position="HB"
          onClick={() => this.props.onClick("HB2")}
        />
        <Player
          empty={roster.WR === undefined}
          data={roster.WR}
          position="WR"
          onClick={() => this.props.onClick("WR")}
        />
        <Player
          empty={roster.WR2 === undefined}
          data={roster.WR2}
          position="WR"
          onClick={() => this.props.onClick("WR2")}
        />
        <Player
          empty={roster.WR3 === undefined}
          data={roster.WR3}
          full={false}
          position="WR"
          onClick={() => this.props.onClick("WR3")}
        />
        <Player
          empty={roster.TE === undefined}
          data={roster.TE}
          position="TE"
          onClick={() => this.props.onClick("TE")}
        />
        <Player
          empty={roster.FLEX === undefined}
          data={roster.FLEX}
          position="Flex"
          onClick={() => this.props.onClick("FLEX")}
        />

        <FormContainer>
          <form onSubmit={this.handleSubmit}>
            <InputBlock>
              <label>Enter Your Submission Code</label>
              <input
                name="code"
                type="text"
                placeholder="Submission Code"
                value={this.state.code}
                onChange={e => this.handleChange(e, "code")}
              />
            </InputBlock>
            <InputBlock>
              <label>Enter Your Team Name</label>
              <input
                name="teamName"
                type="text"
                placeholder="Team Name"
                value={this.state.teamName}
                onChange={e => this.handleChange(e, "teamName")}
              />
            </InputBlock>

            <p>If you have already made a submission and you submit another your previous submission will be deleted</p>
            <Submit type="submit" />
          </form>
        </FormContainer>
      </Container>
    );
  }
}

const SubSal = styled.div`
  display: flex;
`;

const SalaryContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 5px;
`;

const SalaryCol = styled.div`
  display: flex;
  flex-direction: column;
  text-align: right;
  :first-of-type {
    border-right: 1px solid white;
    margin-right: 5px;
    padding-right: 5px;
  }
  span {
    font-size: 24px;
    font-weight: bold;
  }
  div {
    font-size: 12px;
  }
`;
const Container = styled.div`
  overflow: scroll;
  grid-area: sidebar;
`;

const InputBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
  input {
    margin-top: 5px;
    height: 24px;
    border: none;
    padding-left: 5px;
    border-radius: 2px;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.16), 0 0 0 1px rgba(0, 0, 0, 0.08);
  }
`;

const Submit = styled.input`
  height: 40px;
  width: 100%;
  background: #7e0100;
  font-size: 20px;
  color: #f1edef;
  border: none;
  border-radius: 2px;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.16), 0 0 0 1px rgba(0, 0, 0, 0.08);
  cursor: pointer;
  :focus {
    outline: none;
  }
`;

const FormContainer = styled.div`
  margin-top: 5px;
  p {
    color: #f1edef;
    font-size: 14px;
  }
`;

export default PlayerList;
